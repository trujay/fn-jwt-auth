package simple

import (
	"fmt"
	"os"

	"crypto/rsa"
	b64 "encoding/base64"

	"github.com/fnproject/fn/api/server"
	"github.com/fnproject/fn/fnext"
	jwt "gopkg.in/dgrijalva/jwt-go.v3"
)

func init() {
	server.RegisterExtension(&JWTAuth{})
}

const (
	RSAPubKey = "RSAKeyB64"
)

type JWTAuth struct {
	pubKey *rsa.PublicKey
	method jwt.SigningMethod
}

func (e *JWTAuth) Name() string {
	return "bitbucket.org/trujay/fn-jwt-auth"
}

func (e *JWTAuth) Setup(s fnext.ExtServer) error {
	fmt.Println("SETTING UP JWT AUTH")

	return nil
}

func (e *JWTAuth) SetupV2(s fnext.ExtServer) error {
	fmt.Println("SETTING UP JWT AUTH")
	if os.Getenv(RSAPubKey) == "" {
		return fmt.Errorf("%s env var is required for RSA Public Key auth extension", RSAPubKey)
	}

	keyBytes, err := b64.StdEncoding.DecodeString(os.Getenv(RSAPubKey))
	if err != nil {
		return fmt.Errorf("%s env var could not be b64 decoded. %s", RSAPubKey, err)
	}
	parsedKey, err := jwt.ParseRSAPublicKeyFromPEM(keyBytes)
	if err != nil {
		return fmt.Errorf("%s env var could not be b64 decoded. %s", RSAPubKey, err)
	}

	method := jwt.GetSigningMethod("RS256")
	jwtAuth := &JWTAuth{method: method, pubKey: parsedKey}
	s.AddAPIMiddleware(&JWTMiddleware{jwtAuth: jwtAuth})

	return nil
}
