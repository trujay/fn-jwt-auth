package simple

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/fnproject/fn/api/server"
	jwt "gopkg.in/dgrijalva/jwt-go.v3"
)

type JWTMiddleware struct {
	jwtAuth *JWTAuth
}

func (m *JWTMiddleware) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		authorizationHeader := r.Header.Get("Authorization")
		if authorizationHeader == "" {
			server.WriteError(ctx, w, http.StatusUnauthorized, errors.New("No Authorization header, access denied"))
			return
		}

		// Authorization: bearer SomeValue
		ahSplit := strings.Split(authorizationHeader, " ")
		if len(ahSplit) != 2 || strings.ToLower(ahSplit[0]) != "bearer" {
			server.WriteError(ctx, w, http.StatusUnauthorized, errors.New("Invalid authorization header, access denied"))
			return
		}
		token, err := jwt.Parse(ahSplit[1], func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			claims, ok := token.Claims.(jwt.MapClaims)
			if !ok {
				return nil, fmt.Errorf("Token claims not readable: %s", token)
			}

			fiveMinutesAgo := time.Now().Add(-5 * time.Minute)
			if !claims.VerifyNotBefore(fiveMinutesAgo.Unix(), true) {
				return nil, fmt.Errorf("Claims failed not before test")
			}
			return m.jwtAuth.pubKey, nil
		})
		if err != nil {
			server.WriteError(ctx, w, http.StatusUnauthorized, err)
			return
		}
		if !token.Valid {
			server.WriteError(ctx, w, http.StatusUnauthorized, errors.New("Invalid authorization token, access denied"))
			return
		}
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
