# A JWT auth extension

JWT authentication extension for Fn.

## Usage

Simply add `bitbucket.org/trujay/fn-ext-jwt` to your ext.yaml file and build. [Learn more](https://github.com/fnproject/fn/blob/master/docs/operating/extending.md).

Required environment variables:

* RSA_KEY_B64 - a base64 encoded RSA public key to decode JWT tokens.

Try running the following commands to try it out:

```sh
# First, let's try to access and endpoint without credentials.
curl http://localhost:8080/v1/apps
# Fails... :(

# deploy a function
fn init --runtime go gofunc
cd gofunc
fn deploy --app myapp --local
# SHOULD FAIL

fn stop
fn start -e RSA_KEY_B64=YOUR_RSA_KEY_B64

curl -H "Authorization: Bearer YOUR_TOKEN" http://localhost:8080/v1/deploy
# SHOULD WORK

```